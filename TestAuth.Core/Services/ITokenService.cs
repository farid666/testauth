﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestAuth.Core.Dtos;
using TestAuth.Core.Models;

namespace TestAuth.Core.Services
{
    public interface ITokenService
    {
        TokenDto CreateAccessToken(AppUser appUser);
    }
}
