﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TestAuth.Core.Dtos;

namespace TestAuth.Core.Services
{
    public interface IGenericService<TEntity,TDto> where TEntity:class where TDto:class
    {
        Task<Response<TDto>> GetByIdAsync(int id);

        Task<Response<IEnumerable<TDto>>> GetAllAsync();

        Response<IQueryable<TDto>> Where(Expression<Func<TEntity,bool>> predicate);

        Task<Response<NoDataDto>> Remove(int id);

        Task<Response<TDto>> AddAsync(TDto dto);

        Task<Response<TDto>> Update(TDto dto,int id);

    }
}
