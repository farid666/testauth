﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestAuth.Core.Dtos;

namespace TestAuth.Core.Services
{
    public interface IAuthenticationService
    {
        Task<Response<TokenDto>> CreateAccessTokenAsync(LoginDto loginDto);

        Task<Response<TokenDto>> CreateAccessTokenByRefreshTokenAsync(string refreshToken);

        Task<Response<NoDataDto>> RevokeRefreshTokenAsync(string refreshToken);
    }
}
