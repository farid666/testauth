﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAuth.Core.Configurations
{
    public class CustomTokenOptions
    {
        public List<String> Audiences { get; set; }
        public string Issuer { get; set; }
        public int AccessTokenExpireDate { get; set; }
        public int RefreshTokenExpireDate { get; set; }
        public string SecurityKey { get; set; }
    }
}
