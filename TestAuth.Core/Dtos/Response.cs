﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAuth.Core.Dtos
{
    public class Response<T> where T:class
    {
        public T Data { get; private set; }
        public int StatusCode { get; set; }
        public ErrorDto ErrorDto { get; set; }

        

        public static Response<T> Success(T data,int statusCode)
        {
            return new Response<T> { Data = data,StatusCode = statusCode };
        }

        public static Response<T> Success(int statusCode)
        {
            return new Response<T> {Data = default,StatusCode = statusCode };
        }

        public static Response<T> Fail(string error,int statusCode,bool isShow)
        {
            ErrorDto errorDto = new ErrorDto(error,isShow);
            return new Response<T> { ErrorDto = errorDto, StatusCode = statusCode };
        }

        public static Response<T> Fail(ErrorDto errorDto,int statusCode)
        {
            return new Response<T> { ErrorDto = errorDto, StatusCode = statusCode };
        }


    }
}
