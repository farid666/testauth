﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAuth.Core.Dtos
{
    public class ErrorDto
    {
        public List<String> Errors { get; set; } = new List<string>();
        public bool IsShow { get; set; }


        public ErrorDto(string error,bool isShow)
        {
            Errors.Add(error);
            IsShow = isShow;
        }

        public ErrorDto(List<String> errors,bool isShow)
        {
            Errors = errors;
            IsShow = isShow;
        }


    }
}
