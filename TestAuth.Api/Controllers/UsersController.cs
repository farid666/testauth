﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestAuth.Core.Dtos;
using TestAuth.Core.Services;

namespace TestAuth.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : CustomBaseController
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateUser(CreateUserDto createUserDto)
        {
            var response = await _userService.CreateUserAsync(createUserDto);

            return ActionResultInstance(response);
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> GetUserByUserName()
        {
            var response = await _userService.GetUserByUserName(HttpContext.User.Identity.Name);

            return ActionResultInstance(response);
        }
    }
}
