﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestAuth.Core.Dtos;
using TestAuth.Core.Models;
using TestAuth.Core.Services;

namespace TestAuth.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : CustomBaseController
    {
        private readonly IGenericService<Product,ProductDto> _genericProductService;

        public ProductsController(IGenericService<Product, ProductDto> genericProductService)
        {
            _genericProductService = genericProductService;
        }

        [HttpGet]
        public async Task<IActionResult> GetList()
        {
            return ActionResultInstance(await _genericProductService.GetAllAsync());
        }

        [HttpGet("{productId}")]
        public async Task<IActionResult> GetProductById(int productId)
        {
            var product = await _genericProductService.GetByIdAsync(productId);

            return ActionResultInstance(product);
        }

        [HttpPost]
        public async Task<IActionResult> AddProduct(ProductDto productDto)
        {
            var product = await _genericProductService.AddAsync(productDto);

            return ActionResultInstance(product);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateProduct(ProductDto productDto)
        {
            return ActionResultInstance(await _genericProductService.Update(productDto, productDto.Id));
        }

        [HttpDelete("{productId}")]
        public async Task<IActionResult> DeleteProduct(int productId)
        {
            return ActionResultInstance(await _genericProductService.Remove(productId));
        }
    }
}
