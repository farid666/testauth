﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestAuth.Core.Dtos;
using TestAuth.Core.Services;

namespace TestAuth.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AuthsController : CustomBaseController
    {
        private readonly IAuthenticationService _authenticationService;

        public AuthsController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateToken(LoginDto loginDto)
        {
            var response = await _authenticationService.CreateAccessTokenAsync(loginDto);

            return ActionResultInstance(response);
        }
        [HttpPost]
        public async Task<IActionResult> CreateTokenByRefreshToken(RefreshTokenDto refreshTokenDto)
        {
            var response = await _authenticationService.CreateAccessTokenByRefreshTokenAsync(refreshTokenDto.RefreshToken);

            return ActionResultInstance(response);
        }

        [HttpPost]
        public async Task<IActionResult> RevokeRefreshToken(RefreshTokenDto refreshTokenDto)
        {
            var response = await _authenticationService.RevokeRefreshTokenAsync(refreshTokenDto.RefreshToken);

            return ActionResultInstance(response);
        }

    }
}
