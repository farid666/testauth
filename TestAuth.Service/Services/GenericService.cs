﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TestAuth.Core.Dtos;
using TestAuth.Core.Repositories;
using TestAuth.Core.Services;
using TestAuth.Core.UnitOfWork;

namespace TestAuth.Service.Services
{
    public class GenericService<TEntity, TDto> : IGenericService<TEntity, TDto> where TEntity : class where TDto : class
    {
        private readonly IGenericRepository<TEntity> _genericRepository;
        private readonly IUnitOfWork _unitOfWork;

        public GenericService(IGenericRepository<TEntity> genericRepository, IUnitOfWork unitOfWork)
        {
            _genericRepository = genericRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<Response<TDto>> AddAsync(TDto dto)
        {
            var entity = ObjectMapper.Mapper.Map<TEntity>(dto);
            await _genericRepository.AddAsync(entity);

            await _unitOfWork.CommitAsync();

            var newDto = ObjectMapper.Mapper.Map<TDto>(entity);

            return Response<TDto>.Success(newDto,200);
        }

        public async Task<Response<IEnumerable<TDto>>> GetAllAsync()
        {
            var entityList = await _genericRepository.GetAllAsync();

            var listDto = ObjectMapper.Mapper.Map<IEnumerable<TDto>>(entityList);

            return Response<IEnumerable<TDto>>.Success(listDto,200);
        }

        public async Task<Response<TDto>> GetByIdAsync(int id)
        {
            var entity = await _genericRepository.GetByIdAsync(id);

            if (entity == null)
            {
                return Response<TDto>.Fail("Id not found",404,true);
            }

            var dto = ObjectMapper.Mapper.Map<TDto>(entity);

            return Response<TDto>.Success(dto, 200);
        }

        public async Task<Response<NoDataDto>> Remove(int id)
        {
            var entity = await _genericRepository.GetByIdAsync(id);

            if (entity == null)
            {
                return Response<NoDataDto>.Fail("Id not found", 404, true);
            }

            _genericRepository.Remove(entity);

            await _unitOfWork.CommitAsync();

            return Response<NoDataDto>.Success(200);
        }

        public async Task<Response<TDto>> Update(TDto dto, int id)
        {
            var entity = await _genericRepository.GetByIdAsync(id);

            if (entity == null)
            {
                return Response<TDto>.Fail("Id not found", 404, true);
            }

            var newEntity = ObjectMapper.Mapper.Map<TEntity>(dto);

            var  updatedEntity = _genericRepository.Update(newEntity);

            var newDto = ObjectMapper.Mapper.Map<TDto>(updatedEntity);

            _unitOfWork.Commit();

            return Response<TDto>.Success(newDto, 200);
        }

        public Response<IQueryable<TDto>> Where(Expression<Func<TEntity, bool>> predicate)
        {
            var list = _genericRepository.Where(predicate);

            var dtoList = ObjectMapper.Mapper.Map<IQueryable<TDto>>(list);

            return Response<IQueryable<TDto>>.Success(dtoList,200);
        }
    }
}
