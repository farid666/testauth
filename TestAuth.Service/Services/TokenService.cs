﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using TestAuth.Core.Configurations;
using TestAuth.Core.Dtos;
using TestAuth.Core.Models;
using TestAuth.Core.Services;

namespace TestAuth.Service.Services
{
    public class TokenService : ITokenService
    {
        private readonly CustomTokenOptions _customTokenOptions;

        public TokenService(IOptions<CustomTokenOptions> options)
        {
            _customTokenOptions = options.Value;
        }


        private string CreateRefreshToken()
        {
            var numberByte = new Byte[32];

            using var rnd = RandomNumberGenerator.Create();

            rnd.GetBytes(numberByte);

            return Convert.ToBase64String(numberByte);
        }

        private IEnumerable<Claim> GetClaims(AppUser appUser,List<String> audiences) 
        {
            var userList = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier,appUser.Id),
                new Claim(ClaimTypes.Email,appUser.Email),
                new Claim(ClaimTypes.Name,appUser.UserName)
            };

            userList.AddRange(audiences.Select(x=> new Claim(JwtRegisteredClaimNames.Aud,x)));

            return userList;
        }

        public TokenDto CreateAccessToken(AppUser appUser)
        {
            var accessTokenExpireDate = DateTime.Now.AddMinutes(_customTokenOptions.AccessTokenExpireDate);
            var refreshTokenExpireDate = DateTime.Now.AddMinutes(_customTokenOptions.RefreshTokenExpireDate);

            var securityKey = SignService.GetSymmetricSecurityKey(_customTokenOptions.SecurityKey);

            SigningCredentials signingCredentials = new SigningCredentials(securityKey,SecurityAlgorithms.HmacSha256Signature);

            JwtSecurityToken jwtSecurityToken = new JwtSecurityToken(
                issuer:_customTokenOptions.Issuer,
                expires:accessTokenExpireDate,
                notBefore:DateTime.Now,
                signingCredentials:signingCredentials,
                claims:GetClaims(appUser,_customTokenOptions.Audiences)
                );

            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.WriteToken(jwtSecurityToken);

            var tokenDto = new TokenDto
            {
                AccessToken = token,
                AccessTokenExpireDate = accessTokenExpireDate,
                RefreshToken = CreateRefreshToken(),
                RefreshTokenExpireDate = refreshTokenExpireDate
            };

            return tokenDto;
        }
    }
}
