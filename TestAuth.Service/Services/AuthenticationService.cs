﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestAuth.Core.Dtos;
using TestAuth.Core.Models;
using TestAuth.Core.Repositories;
using TestAuth.Core.Services;
using TestAuth.Core.UnitOfWork;

namespace TestAuth.Service.Services
{
    public class AuthenticationService : IAuthenticationService
    {

        private readonly ITokenService _tokenService;
        private readonly UserManager<AppUser> _userManager;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGenericRepository<UserRefreshToken> _genericUserRefreshTokenRepository;

        public AuthenticationService(ITokenService tokenService, UserManager<AppUser> userManager
        , IUnitOfWork unitOfWork, IGenericRepository<UserRefreshToken> genericUserRefreshTokenRepository)
        {
            _userManager = userManager;
            _unitOfWork = unitOfWork;
            _tokenService = tokenService;
            _genericUserRefreshTokenRepository = genericUserRefreshTokenRepository;
        }

        public async Task<Response<TokenDto>> CreateAccessTokenAsync(LoginDto loginDto)
        {
            if (loginDto == null)
            {
                throw new ArgumentNullException(nameof(loginDto));
            }

            var user = await _userManager.FindByEmailAsync(loginDto.Email);

            if (user == null)
            {
                return Response<TokenDto>.Fail("Email or Password invalid!", 400, true);
            }

            if (!await _userManager.CheckPasswordAsync(user,loginDto.Password))
            {
                return Response<TokenDto>.Fail("Email or Password invalid!", 400, true);
            }

            var tokenDto = _tokenService.CreateAccessToken(user);

            var userRefreshToken = _genericUserRefreshTokenRepository.Where(x => x.UserId == user.Id).SingleOrDefault();

            if (userRefreshToken == null)
            {
                await _genericUserRefreshTokenRepository.AddAsync(new UserRefreshToken {UserId = user.Id, RefreshToken = tokenDto.RefreshToken, ExpireDate = tokenDto.RefreshTokenExpireDate });
            }
            else
            {
                userRefreshToken.RefreshToken = tokenDto.RefreshToken;
                userRefreshToken.ExpireDate = tokenDto.RefreshTokenExpireDate;
            }

            await _unitOfWork.CommitAsync();

            return Response<TokenDto>.Success(tokenDto,200);
        }

        public async Task<Response<TokenDto>> CreateAccessTokenByRefreshTokenAsync(string refreshToken)
        {
            var existUserRefreshToken = _genericUserRefreshTokenRepository.Where(x => x.RefreshToken == refreshToken).SingleOrDefault();

            if (existUserRefreshToken == null)
            {
                return Response<TokenDto>.Fail("RefreshToken not found",404,true);
            }

            var user = await _userManager.FindByIdAsync(existUserRefreshToken.UserId);

            if (user == null)
            {
                return Response<TokenDto>.Fail("User Id not found", 404, true);
            }

            var tokenDto = _tokenService.CreateAccessToken(user);

            existUserRefreshToken.RefreshToken = tokenDto.RefreshToken;
            existUserRefreshToken.ExpireDate = tokenDto.RefreshTokenExpireDate;

            await _unitOfWork.CommitAsync();

            return Response<TokenDto>.Success(tokenDto,200);
        }

        public async Task<Response<NoDataDto>> RevokeRefreshTokenAsync(string refreshToken)
        {
            var existUserRefreshToken = _genericUserRefreshTokenRepository.Where(x => x.RefreshToken == refreshToken).SingleOrDefault();

            if (existUserRefreshToken == null)
            {
                return Response<NoDataDto>.Fail("RefreshToken not found", 404, true);
            }

            _genericUserRefreshTokenRepository.Remove(existUserRefreshToken);

            await _unitOfWork.CommitAsync();

            return Response<NoDataDto>.Success(200);
        }
    }
}
